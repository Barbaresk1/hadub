#!/bin/bash

hadoop fs -mkdir /hw1/
hadoop fs -mkdir /hw1/input
hadoop fs -mkdir /hw1/dict

hadoop fs -rm /hw1/input/*
hadoop fs -rm /hw1/dict/*

hadoop fs -put ./input/imp.* /hw1/input
hadoop fs -put ./input/*.en.txt /hw1/dict
