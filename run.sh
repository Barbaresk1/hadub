#!/bin/bash

hadoop fs -rm -r /hw1/output

echo "[$(date '+%Y-%m-%d %H:%M:%S')] Job started."

hadoop jar ./target/hw1-1.0-SNAPSHOT-jar-with-dependencies.jar /hw1/input /hw1/output /hw1/dict/city.en.txt /hw1/dict/region.en.txt

[ "$?" -ne "0" ] && {
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] --- ERROR ---"
	exit
}

echo "[$(date '+%Y-%m-%d %H:%M:%S')] Job finished."

#rm -rf ./output/*
#mkdir ./output

echo " RESULTS: "
echo ""

hadoop fs -text /hw1/output/part-r-*

echo ""
echo " END."
