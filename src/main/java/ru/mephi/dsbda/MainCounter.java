package ru.mephi.dsbda;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import java.net.URI;
import java.util.logging.Logger;

/**
 * MainCounter.java
 */
public class MainCounter {

    /**
     * точка входа.
     * args входные аргументы
     * Если аргументов меньше 4, выбрасываем runtimeException
     */
    public static void main(String[] args) throws Exception {

//
//        if (args.length < 4) {
//            throw new RuntimeException("illeagal parametrs");
//        }

        Logger log = Logger.getLogger(MainCounter.class.getName());

        log.info("hello");
        /// Конфигурация
        Configuration conf = new Configuration();

        // создаем job
        Job job = Job.getInstance(conf, "BDA HW1 Counter");

        // Редьюсеры
        job.setNumReduceTasks(15);

        job.setJarByClass(MainCounter.class);
        job.setMapperClass(MainMapper.class);
        job.setCombinerClass(MainCombiner.class);
        job.setReducerClass(MainReducer.class);
        job.setPartitionerClass(MainPartitioner.class);

        // Mapper
        job.setMapOutputKeyClass(LogStringData.class);
        job.setMapOutputValueClass(IntWritable.class);

        // Выходные (Reducer)
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);


        // Формат - seq(SequenceFileOutputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        //SequenceFileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);

        // Входные и Выходные пути к данным, берем их cmd аргументов
        FileInputFormat.addInputPath(job, new Path(args[1]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));
        //FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        // Кэшируем файлы для Reduce side join
        job.addCacheFile(new URI(args[3]));
        job.addCacheFile(new URI(args[4]));
       // job.addCacheFile(new URI("./City/city.en.txt"));
        //job.addCacheFile(new URI("./City/region.en.txt"));

        long startTime = System.nanoTime();
        //Выполнение
        log.info("==================================JOB START========================");
        job.waitForCompletion(true);
        long estimatedTime = System.nanoTime() - startTime;
        log.info("Spent Time:   " + estimatedTime);
        log.info("==================================JOB END========================");
    }
}
