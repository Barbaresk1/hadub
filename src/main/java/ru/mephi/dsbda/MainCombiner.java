package ru.mephi.dsbda;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class MainCombiner extends Reducer<LogStringData, IntWritable, LogStringData, IntWritable> {

    @Override
    public void reduce(LogStringData key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable value : values) {
            sum += value.get();
        }
        IntWritable temp = new IntWritable(sum);

        context.write(key, temp);

    }
}
