package ru.mephi.dsbda;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

/**
 * LogStringDataTest.java
 * Unit tests for LogStringData class.
 */
public class LogStringDataTest {

    private static final int ID = 42;
    private static final String OS = "MacOS";

    /**
     * Empty Constructor Test + Getters.
     */
    @Test
    public void EmptyConstructorTest() {
        LogStringData test = new LogStringData();
        assertEquals(-1, test.getCityId());
        assertNull(null, test.getOsName());
    }

    /**
     * Full Constructor Test + Getters.
     */
    @Test
    public void FullConstructorTest() {
        LogStringData test = new LogStringData(ID, OS);
        assertEquals(ID, test.getCityId());
        assertEquals(OS, test.getOsName());
    }

    /**
     * Hash generation test.
     */
    @Test
    public void hashCodeTest() {
        LogStringData test1 = new LogStringData(ID, OS);
        LogStringData test2 = new LogStringData(ID + 13, "Linux");
        assertNotEquals(test1.hashCode(), test2.hashCode());
    }

    /**
     * Equals test.
     */
    @Test
    public void EqualsTest() {
        LogStringData test1 = new LogStringData(ID, OS);
        LogStringData test2 = new LogStringData(ID + 13, "Linux");
        LogStringData test3 = new LogStringData(ID , OS);
        LogStringData test4 = test1;
        assertEquals(false, test1.equals("Some object"));
        assertEquals(false, test1.equals(test2));
        assertEquals(true, test1.equals(test3));
        assertEquals(true, test1.equals(test4));
        assertEquals(true, test1.equals(test1));
    }

    /**
     * CompareTo test.
     */
    @Test
    public void compareToTest() {
        LogStringData test1 = new LogStringData(ID, OS);
        LogStringData test2 = new LogStringData(ID + 13, "Linux");
        LogStringData test3 = new LogStringData(ID , OS);
        assertEquals(-1, test1.compareTo(test2));
        assertEquals(1, test2.compareTo(test1));
        assertEquals(0, test1.compareTo(test3));
        assertEquals(0, test1.compareTo(test1));
    }
}
