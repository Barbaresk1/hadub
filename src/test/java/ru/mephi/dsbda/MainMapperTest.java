package ru.mephi.dsbda;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * MainMapperTest.java
 * Main Mapper Test class.
 */
public class MainMapperTest {

    private MapDriver<Object, Text, LogStringData, IntWritable> mapDriver;

    @Before
    public void setUp() {
        mapDriver = MapDriver.newMapDriver(new MainMapper());
    }

    /**
     * Correct Mapper test.
     * @throws IOException
     */
    @Test
    public void CorrectMapperTest() throws IOException {
        mapDriver.withInput(
                new IntWritable(),
                new Text("49b5a58c3a3c39c32ba5a36f70a3daa5\t20131021192300738\t1\tC4NDR12~1B\tMozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)\t222.218.230.*\t238\t246\t1\t9a2acabba41e00373f68053fd73e4d50\t19bbcb08106cbd6410a51fe87c40c44e\tnull\tmm_12712107_1718689_6742002\t300\t250\tFirstView\tNa\t0\t10722\t294\t255\tnull\t2821\t10063")
        );
        mapDriver.withOutput(
                new LogStringData(246, "Windows XP"),
                new IntWritable(1)
        );
        mapDriver.runTest();
    }

    /**
     * Lower price Mapper test.
     * @throws IOException
     */
    @Test
    public void LowPriceMapperTest() throws IOException {
        mapDriver.withInput(
                new IntWritable(),
                new Text("49b5a58c3a3c39c32ba5a36f70a3daa5\t20131021192300738\t1\tC4NDR12~1B\tMozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)\t222.218.230.*\t238\t246\t1\t9a2acabba41e00373f68053fd73e4d50\t19bbcb08106cbd6410a51fe87c40c44e\tnull\tmm_12712107_1718689_6742002\t300\t250\tFirstView\tNa\t0\t10722\t166\t201\tnull\t2821\t10063")
        );
        mapDriver.runTest();
    }

    /**
     * Incorrect price Mapper test.
     * @throws IOException
     */
    @Test
    public void IncorrectPriceMapperTest() throws IOException {
        mapDriver.withInput(
                new IntWritable(),
                new Text("49b5a58c3a3c39c32ba5a36f70a3daa5\t20131021192300738\t1\tC4NDR12~1B\tMozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)\t222.218.230.*\t238\t246\t1\t9a2acabba41e00373f68053fd73e4d50\t19bbcb08106cbd6410a51fe87c40c44e\tnull\tmm_12712107_1718689_6742002\t300\t250\tFirstView\tNa\t0\t10722\tlol\t201\tnull\t2821\t10063")
        );
        mapDriver.runTest();
    }

    /**
     * Incorrect cityId Mapper test.
     * @throws IOException
     */
    @Test
    public void IncorrectCityIdMapperTest() throws IOException {
        mapDriver.withInput(
                new IntWritable(),
                new Text("49b5a58c3a3c39c32ba5a36f70a3daa5\t20131021192300738\t1\tC4NDR12~1B\tMozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0)\t222.218.230.*\t238\tTrololo\t1\t9a2acabba41e00373f68053fd73e4d50\t19bbcb08106cbd6410a51fe87c40c44e\tnull\tmm_12712107_1718689_6742002\t300\t250\tFirstView\tNa\t0\t10722\t666\t201\tnull\t2821\t10063")
        );
        mapDriver.runTest();
    }
}
