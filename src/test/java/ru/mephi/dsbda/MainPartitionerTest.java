package ru.mephi.dsbda;

import org.apache.hadoop.io.IntWritable;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * MainPartitionerTest.java
 * MainPartitioner Test class.
 */
public class MainPartitionerTest {

    private MainPartitioner partitioner;
    private LogStringData logStringData1;
    private LogStringData logStringData2;
    private IntWritable intWritable;

    /**
     * Creates all data before each try.
     */
    @Before
    public void setUp() {
        partitioner = new MainPartitioner();
        logStringData1 = new LogStringData(6, "Linux");
        logStringData2 = new LogStringData(7, "MacOs");
        intWritable = new IntWritable(1);
    }

    /**
     * Partitioner Test with one reducer.
     */
    @Test
    public void IOnePartitionerTest() {
        assertEquals(
                partitioner.getPartition(logStringData1, intWritable, 1),
                partitioner.getPartition(logStringData2, intWritable, 1)
        );
    }

    /**
     * Partitioner Test with two reducers.
     */
    @Test
    public void ITwoPartitionerTest() {
        assertNotEquals(
                partitioner.getPartition(logStringData1, intWritable, 2),
                partitioner.getPartition(logStringData2, intWritable, 2)
        );
    }

    /**
     * Partitioner Test with ten reducers.
     */
    @Test
    public void IPartitionerTest() {
        assertNotEquals(
                partitioner.getPartition(logStringData1, intWritable, 15),
                partitioner.getPartition(logStringData2, intWritable, 15)
        );
    }
}
